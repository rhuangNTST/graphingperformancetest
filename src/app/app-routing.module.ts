import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'angular-fs',
    loadChildren: './modules/ang-fus-charts/ang-fus-charts.module#AngFusChartsModule'
  },
  {
    path: 'hcharts',
    loadChildren: './modules/high-charts-test/high-charts-test.module#HighChartsTestModule'
  },
  {
    path: 'chartjs',
    loadChildren: './modules/chart-js/chart-js.module#ChartJSModule'
  },
  {
    path: 'ng2charts',
    loadChildren: './modules/ng2-charts-test/ng2-charts-test.module#Ng2ChartsTestModule'
  },
  {
    path: 'ngxcharts',
    loadChildren: './modules/ngx/ngx.module#NgxModule'
  },
  {
    path: 'canvasjs',
    loadChildren: './modules/canvas-js/canvas-js.module#CanvasJsModule'
  },
  {
    path: 'ngxecharts',
    loadChildren: './modules/ngx-e/ngx-e.module#NgxEModule'
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
