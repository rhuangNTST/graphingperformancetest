import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-high-charts-test',
  templateUrl: './high-charts-test.component.html',
  styleUrls: []
})
export class HighChartsTestComponent implements OnInit {

  Highcharts = Highcharts; // required
  chartConstructor = 'chart'; // optional string, defaults to 'chart'
  chartOptions = {
    series: [{
      type: 'bar',
      data: [
        [0, 10, 112]
      ] 
    }],
    plotOptions: {
      series: {
        animation: false
      }
    }
    
   }; // required
  updateFlag = false; // optional boolean
  oneToOneFlag = false; // optional boolean, defaults to false
  runOutsideAngularFlag = false; // optional boolean, defaults to false

  constructor() { }

  ngOnInit() {
  }

}
