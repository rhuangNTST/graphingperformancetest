import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighChartsTestComponent } from './high-charts-test.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:'', component: HighChartsTestComponent
  }
]

@NgModule({
  declarations: [HighChartsTestComponent],
  imports: [
    CommonModule,
    HighchartsChartModule,
    RouterModule.forChild(routes)
  ]
})
export class HighChartsTestModule { }
