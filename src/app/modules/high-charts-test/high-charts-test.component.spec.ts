import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighChartsTestComponent } from './high-charts-test.component';

describe('HighChartsTestComponent', () => {
  let component: HighChartsTestComponent;
  let fixture: ComponentFixture<HighChartsTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighChartsTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighChartsTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
