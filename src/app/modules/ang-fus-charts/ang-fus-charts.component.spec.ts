import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngFusChartsComponent } from './ang-fus-charts.component';

describe('AngFusChartsComponent', () => {
  let component: AngFusChartsComponent;
  let fixture: ComponentFixture<AngFusChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngFusChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngFusChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
