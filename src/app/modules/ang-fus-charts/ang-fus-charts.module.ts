import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngFusChartsComponent } from './ang-fus-charts.component';
import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:'', component: AngFusChartsComponent
  }
]

FusionChartsModule.fcRoot(FusionCharts, Charts);

@NgModule({
  declarations: [AngFusChartsComponent],
  imports: [
    CommonModule,
    FusionChartsModule,
    RouterModule.forChild(routes)
  ]
})
export class AngFusChartsModule { }
