import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ang-fus-charts',
  templateUrl: './ang-fus-charts.component.html',
  styleUrls: ['./ang-fus-charts.component.css']
})
export class AngFusChartsComponent implements OnInit {

  data: Object;

  constructor() { 
    this.data = {
      chart: {
        animation: 0,
        caption: "Test",
        xAxisName: "TestX",
        yAxisName: "TestY",
        lineThickness: "1",
       },
      data: [{'value': 68}, {'value': 41}, {'value': 195}, {'value': 117}, {'value': 44}, {'value': 26}, {'value': 98}, {'value': 181}, {'value': 33}, {'value': 182}, {'value': 107}, {'value': 92}, {'value': 173}, {'value': 141}, {'value': 35}, {'value': 26}, {'value': 122}, {'value': 93}, {'value': 100}, {'value': 45}, {'value': 199}, {'value': 133}, {'value': 52}, {'value': 125}, {'value': 94}, {'value': 149}, {'value': 117}, {'value': 66}, {'value': 169}, {'value': 82}, {'value': 99}, {'value': 166}, {'value': 87}, {'value': 75}, {'value': 95}, {'value': 51}, {'value': 132}, {'value': 188}, {'value': 158}, {'value': 185}, {'value': 146}, {'value': 50}, {'value': 125}, {'value': 88}, {'value': 149}, {'value': 131}, {'value': 4}, {'value': 193}, {'value': 75}, {'value': 21}, {'value': 173}, {'value': 42}, {'value': 197}, {'value': 157}, {'value': 177}, {'value': 165}, {'value': 127}, {'value': 175}, {'value': 75}, {'value': 106}, {'value': 160}, {'value': 6}, {'value': 178}, {'value': 4}, {'value': 160}, {'value': 138}, {'value': 85}, {'value': 59}, {'value': 8}, {'value': 157}, {'value': 3}, {'value': 152}, {'value': 49}, {'value': 170}, {'value': 82}, {'value': 24}, {'value': 151}, {'value': 194}, {'value': 153}, {'value': 60}, {'value': 163}, {'value': 126}, {'value': 115}, {'value': 64}, {'value': 40}, {'value': 161}, {'value': 147}, {'value': 56}, {'value': 50}, {'value': 117}, {'value': 32}, {'value': 197}, {'value': 23}, {'value': 152}, {'value': 115}, {'value': 98}, {'value': 75}, {'value': 192}, {'value': 40}, {'value': 69}, {'value': 60}, {'value': 126}, {'value': 68}, {'value': 170}, {'value': 193}, {'value': 46}, {'value': 68}, {'value': 14}, {'value': 180}, {'value': 48}, {'value': 18}, {'value': 186}, {'value': 189}, {'value': 198}, {'value': 151}, {'value': 176}, {'value': 83}, {'value': 185}, {'value': 15}, {'value': 106}, {'value': 69}, {'value': 19}, {'value': 112}, {'value': 119}, {'value': 197}, {'value': 166}, {'value': 68}, {'value': 99}, {'value': 122}, {'value': 84}, {'value': 117}, {'value': 137}, {'value': 19}, {'value': 23}, {'value': 178}, {'value': 16}, {'value': 15}, {'value': 197}, {'value':19}, {'value': 200}, {'value': 117}, {'value': 196}, {'value': 138}, {'value': 137}, {'value': 155}, {'value': 41}, {'value': 81}, {'value': 40}, {'value': 113}, {'value': 149}, {'value': 141}, {'value': 71}, {'value': 99}, {'value': 124}, {'value': 116}, {'value': 129}, {'value': 19}, {'value': 50}, {'value': 137}, {'value': 45}, {'value': 79}, {'value':  18}, {'value': 190}, {'value': 100}, {'value': 49}, {'value': 9}, {'value': 126}, {'value': 21}, {'value': 122}, {'value': 198}, {'value': 138}, {'value': 81}, {'value': 183}, {'value': 0}, {'value': 79}, {'value': 163}, {'value': 41}, {'value': 124}, {'value': 158}, {'value': 135}, {'value': 22}, {'value': 145}, {'value': 17}, {'value': 102}, {'value': 101}, {'value': 100}, {'value': 194}, {'value': 44}, {'value': 190}, {'value': 157}, {'value': 19}, {'value': 119}, {'value': 78}, {'value': 87}, {'value': 151}, {'value': 60}, {'value': 119}, {'value': 165}, {'value': 175}, {'value': 10}]

    };
  }

  ngOnInit() {
  }

}
