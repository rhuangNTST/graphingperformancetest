import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxEComponent } from './ngx-e.component';

describe('NgxEComponent', () => {
  let component: NgxEComponent;
  let fixture: ComponentFixture<NgxEComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxEComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxEComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
