import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxEComponent } from './ngx-e.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';

const routes: Routes = [
  {
    path: '', component: NgxEComponent
  }
]

@NgModule({
  declarations: [NgxEComponent],
  imports: [
    CommonModule,
    NgxEchartsModule,
    RouterModule.forChild(routes),
  ]
})
export class NgxEModule { }
