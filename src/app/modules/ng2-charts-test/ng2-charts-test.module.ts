import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2ChartsTestComponent } from './ng2-charts-test.component';
import { ChartsModule } from 'ng2-charts';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'', component: Ng2ChartsTestComponent
  }
]

@NgModule({
  declarations: [Ng2ChartsTestComponent],
  imports: [
    CommonModule,
    ChartsModule,
    RouterModule.forChild(routes)
  ]
})
export class Ng2ChartsTestModule { }
