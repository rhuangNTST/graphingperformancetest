import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ng2ChartsTestComponent } from './ng2-charts-test.component';

describe('Ng2ChartsTestComponent', () => {
  let component: Ng2ChartsTestComponent;
  let fixture: ComponentFixture<Ng2ChartsTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ng2ChartsTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ng2ChartsTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
