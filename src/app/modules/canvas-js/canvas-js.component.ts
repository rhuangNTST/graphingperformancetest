import { Component, OnInit, Input } from '@angular/core';
import * as CanvasJS from './canvasjs.min.js';

@Component({
  selector: 'app-canvas-js',
  templateUrl: './canvas-js.component.html',
  styleUrls: ['./canvas-js.component.css']
})
export class CanvasJsComponent implements OnInit {
  xs = [0, 4, 8, 12];

  @Input() divId: string;
  dataPoints = [{y: 76}, {y: 53}, {y: 132}, {y: 127}, {y: 134}, {y: 29}, {y: 112}, {y: 186}, {y: 183}, {y: 71}, {y: 109}, {y: 113}, {y: 72}, {y: 168}, {y: 20}, {y: 139}, {y: 105}, {y: 115}, {y: 200}, {y: 172}, {y: 166}, {y: 21}, {y: 75}, {y: 188}, {y: 0}, {y: 111}, {y: 24}, {y: 29}, {y: 44}, {y: 117}, {y: 114}, {y: 33}, {y: 188}, {y: 144}, {y: 174}, {y: 67}, {y: 167}, {y: 15}, {y: 167}, {y: 140}, {y: 66}, {y: 114}, {y: 14}, {y: 33}, {y: 168}, {y: 45}, {y: 140}, {y: 169}, {y: 162}, {y: 174}, {y: 4}, {y: 64}, {y: 1}, {y: 137}, {y: 76}, {y: 73}, {y: 45}, {y: 105}, {y: 194}, {y: 75}, {y: 141}, {y: 156}, {y: 116}, {y: 142}, {y: 70}, {y: 198}, {y: 114}, {y: 48}, {y: 97}, {y: 63}, {y: 109}, {y: 129}, {y: 139}, {y: 17}, {y: 103}, {y: 105}, {y: 190}, {y: 66}, {y: 154}, {y: 47}, {y: 160}, {y: 77}, {y: 119}, {y: 5}, {y: 80}, {y: 134}, {y: 174}, {y: 41}, {y: 93}, {y: 82}, {y: 136}, {y: 191}, {y: 125}, {y: 6}, {y: 162}, {y: 32}, {y: 106}, {y: 65}, {y: 39}, {y: 130}, {y: 148}, {y: 11}, {y: 107}, {y: 114}, {y: 38}, {y: 194}, {y: 94}, {y: 49}, {y: 96}, {y: 86}, {y: 121}, {y: 4}, {y: 156}, {y: 183}, {y: 24}, {y: 88}, {y: 84}, {y: 96}, {y: 78}, {y: 84}, {y: 81}, {y: 167}, {y: 195}, {y: 87}, {y: 94}, {y: 22}, {y: 24}, {y: 135}, {y: 67}, {y: 174}, {y: 79}, {y: 109}, {y: 14}, {y: 159}, {y: 176}, {y: 68}, {y: 28}, {y: 66}, {y: 187}, {y: 40}, {y: 65}, {y: 92}, {y: 112}, {y: 164}, {y: 122}, {y: 115}, {y: 191}, {y: 174}, {y: 134}, {y: 26}, {y: 157}, {y: 176}, {y: 52}, {y: 83}, {y: 131}, {y: 97}, {y: 194}, {y: 83}, {y: 21}, {y: 122}, {y: 170}, {y: 66}, {y: 166}, {y: 111}, {y: 34}, {y: 143}, {y: 3}, {y: 182}, {y: 81}, {y: 5}, {y: 200}, {y: 76}, {y: 40}, {y: 187}, {y: 68}, {y: 47}, {y: 156}, {y: 147}, {y: 144}, {y: 37}, {y: 192}, {y: 42}, {y: 21}, {y: 125}, {y: 28}, {y: 29}, {y: 113}, {y: 120}, {y: 24}, {y: 71}, {y: 88}, {y: 154}, {y: 113}, {y: 106}, {y: 55}, {y: 74}, {y: 191}, {y: 19}, {y: 7}, {y: 0}];
  
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    for (let i = 0; i < 16; i++) {  
      let chart = new CanvasJS.Chart(i.toString(), {
        zoomEnabled: true,
        animationEnabled: false,
        exportEnabled: false,
        title: {
          text: "Performance Demo"
        },
        subtitles: [{
          text: "Try Zooming and Panning"
        }],
        data: [
          {
            type: "line",
            dataPoints: this.dataPoints,
          }]
      });
      chart.render();
    }
  }
}
