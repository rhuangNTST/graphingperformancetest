import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CanvasJsComponent } from './canvas-js.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'', component: CanvasJsComponent
  }
]

@NgModule({
  declarations: [CanvasJsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class CanvasJsModule { }
