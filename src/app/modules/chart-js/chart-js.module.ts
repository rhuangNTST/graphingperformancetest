import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { ChartJSComponent } from './chart-js.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'', component: ChartJSComponent
  }
]

@NgModule({
  declarations: [ChartJSComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ChartJSModule { }
