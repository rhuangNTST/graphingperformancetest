import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-chart-js',
  templateUrl: './chart-js.component.html',
})
export class ChartJSComponent implements OnInit {
  title = 'ChartJS'
  public chart;
  public chart2;
  public chart3;
  public chart4;
  public chart5;
  public chart6;
  public chart7
  public chart8;
  public chart9;
  public chart10;
  public chart11;
  public chart12;
  public chart13;
  public chart14;
  public chart15;
  public chart16;
  constructor() { }

  ngOnInit() {
    // Line chart:
    let labelO = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60',
    '61', '62', '63', '64', '65', '66', '67', '68', '69', '70', '71', '72', '73', '74', '75', '76', '77', '78', '79', '80', '81', '82', '83', '84', '85', '86', '87', '88', '89', '90', 
    '91', '92', '93', '94', '95', '96', '97', '98', '99', '100', '101', '102', '103', '104', '105', '106', '107', '108', '109', '110', '111', '112', '113', '114', '115', '116', '117',
     '118', '119', '120', '121', '122', '123', '124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '136', '137', '138', '139', '140', '141', '142', '143',
      '144', '145', '146', '147', '148', '149', '150', '151', '152', '153', '154', '155', '156', '157', '158', '159', '160', '161', '162', '163', '164', '165', '166', '167', '168', 
    '169', '170', '171', '172', '173', '174', '175', '176', '177', '178', '179', '180', '181', '182', '183', '184', '185', '186', '187', '188', '189', '190', '191', '192', '193', 
    '194', '195', '196', '197', '198', '199'];
    let dataO = [68, 41, 195, 117, 44, 26, 98, 181, 33, 182, 107, 92, 173, 141, 35, 26, 122, 93, 100, 45, 199, 133, 52, 125, 94, 149, 117, 66, 169, 82, 99, 166, 87, 75, 95, 51, 132, 188, 158, 
      185, 146, 50, 125, 88, 149, 131, 4, 193, 75, 21, 173, 42, 197, 157, 177, 165, 127, 175, 75, 106, 160, 6, 178, 4, 160, 138, 85, 59, 8, 157, 3, 152, 49, 170, 82, 24, 151, 194, 153, 
      60, 163, 126, 115, 64, 40, 161, 147, 56, 50, 117, 32, 197, 23, 152, 115, 98, 75, 192, 40, 69, 60, 126, 68, 170, 193, 46, 68, 14, 180, 48, 18, 186, 189, 198, 151, 176, 83, 185, 15, 
      106, 69, 19, 112, 119, 197, 166, 68, 99, 122, 84, 117, 137, 19, 23, 178, 16, 15, 197, 19, 200, 117, 196, 138, 137, 155, 41, 81, 40, 113, 149, 141, 71, 99, 124, 116, 129, 19, 50, 
      137, 45, 79, 18, 190, 100, 49, 9, 126, 21, 122, 198, 138, 81, 183, 0, 79, 163, 41, 124, 158, 135, 22, 145, 17, 102, 101, 100, 194, 44, 190, 157, 19, 119, 78, 87, 151, 60, 119, 165, 
      175, 10];
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [ { ticks: { beginAtZero:true } } ]
        }
      }
    });

    this.chart2 = new Chart('canvas2', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart3 = new Chart('canvas3', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart4 = new Chart('canvas4', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart5 = new Chart('canvas5', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart6 = new Chart('canvas6', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart7 = new Chart('canvas7', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart8 = new Chart('canvas8', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart9 = new Chart('canvas9', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart10 = new Chart('canvas10', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart11= new Chart('canvas11', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart12 = new Chart('canvas12', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart13 = new Chart('canvas13', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart14 = new Chart('canvas14', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart15 = new Chart('canvas15', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });

    this.chart16 = new Chart('canvas16', {
      type: 'line',
      data: {
        labels: labelO,
        datasets: [
          {
            label: 'HeartRate Hourly',
            data: dataO,
            fill:false,
            backgroundColor: 'red',
            borderColor: 'red',
          },
        ]
      }, 
      options: {
        animation: {
          duration: 0
        },
        title:{
          text:"Line Chart",
          display:true
        },
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero:true }
            }
          ]
        }
      }
    });
  }

}
