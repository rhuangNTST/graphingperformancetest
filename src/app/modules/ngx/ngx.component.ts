import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngx',
  templateUrl: './ngx.component.html',
  styleUrls: ['./ngx.component.css']
})
export class NgxComponent implements OnInit {
  view: any[] = [400, 225];

  // options
  animations = false;
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Number';
  showYAxisLabel = true;
  yAxisLabel = 'Color Value';
  timeline = false;
  tooltipDisabled = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // data
  multi: any[] = [
    {
      name: 'Green',
      series: [
        { name: 0, value: 68 }, { name: 1, value: 41 }, { name: 2, value: 195 }, { name: 3, value: 117 }, { name: 4, value: 44 }, { name: 5, value: 26 }, { name: 6, value: 98 }, { name: 7, value: 181 }, { name: 8, value: 33 }, { name: 9, value: 182 }, { name: 10, value: 107 }, { name: 11, value: 92 }, { name: 12, value: 173 }, { name: 13, value: 141 }, { name: 14, value: 35 }, { name: 15, value: 26 }, { name: 16, value: 122 }, { name: 17, value: 93 }, { name: 18, value: 100 }, { name: 19, value: 45 }, { name: 20, value: 199 }, { name: 21, value: 133 }, { name: 22, value: 52 }, { name: 23, value: 125 }, { name: 24, value: 94 }, { name: 25, value: 149 }, { name: 26, value: 117 }, { name: 27, value: 66 }, { name: 28, value: 169 }, { name: 29, value: 82 }, { name: 30, value: 99 }, { name: 31, value: 166 }, { name: 32, value: 87 }, { name: 33, value: 75 }, { name: 34, value: 95 }, { name: 35, value: 51 }, { name: 36, value: 132 }, { name: 37, value: 188 }, { name: 38, value: 158 }, { name: 39, value: 185 }, { name: 40, value: 146 }, { name: 41, value: 50 }, { name: 42, value: 125 }, { name: 43, value: 88 }, { name: 44, value: 149 }, { name: 45, value: 131 }, { name: 46, value: 4 }, { name: 47, value: 193 }, { name: 48, value: 75 }, { name: 49, value: 21 }, { name: 50, value: 173 }, { name: 51, value: 42 }, { name: 52, value: 197 }, { name: 53, value: 157 }, { name: 54, value: 177 }, { name: 55, value: 165 }, { name: 56, value: 127 }, { name: 57, value: 175 }, { name: 58, value: 75 }, { name: 59, value: 106 }, { name: 60, value: 160 }, { name: 61, value: 6 }, { name: 62, value: 178 }, { name: 63, value: 4 }, { name: 64, value: 160 }, { name: 65, value: 138 }, { name: 66, value: 85 }, { name: 67, value: 59 }, { name: 68, value: 8 }, { name: 69, value: 157 }, { name: 70, value: 3 }, { name: 71, value: 152 }, { name: 72, value: 49 }, { name: 73, value: 170 }, { name: 74, value: 82 }, { name: 75, value: 24 }, { name: 76, value: 151 }, { name: 77, value: 194 }, { name: 78, value: 153 }, { name: 79, value: 60 }, { name: 80, value: 163 }, { name: 81, value: 126 }, { name: 82, value: 115 }, { name: 83, value: 64 }, { name: 84, value: 40 }, { name: 85, value: 161 }, { name: 86, value: 147 }, { name: 87, value: 56 }, { name: 88, value: 50 }, { name: 89, value: 117 }, { name: 90, value: 32 }, { name: 91, value: 197 }, { name: 92, value: 23 }, { name: 93, value: 152 }, { name: 94, value: 115 }, { name: 95, value: 98 }, { name: 96, value: 75 }, { name: 97, value: 192 }, { name: 98, value: 40 }, { name: 99, value: 69 }, { name: 100, value: 60 }, { name: 101, value: 126 }, { name: 102, value: 68 }, { name: 103, value: 170 }, { name: 104, value: 193 }, { name: 105, value: 46 }, { name: 106, value: 68 }, { name: 107, value: 14 }, { name: 108, value: 180 }, { name: 109, value: 48 }, { name: 110, value: 18 }, { name: 111, value: 186 }, { name: 112, value: 189 },
      ]
    },
    {
      name: 'Red',
      series: [
        { name: 113, value: 198 }, { name: 114, value: 151 }, { name: 115, value: 176 }, { name: 116, value: 83 }, { name: 117, value: 185 }, { name: 118, value: 15 }, { name: 119, value: 106 }, { name: 120, value: 69 }, { name: 121, value: 19 }, { name: 122, value: 112 }, { name: 123, value: 119 }, { name: 124, value: 197 }, { name: 125, value: 166 }, { name: 126, value: 68 }, { name: 127, value: 99 }, { name: 128, value: 122 }, { name: 129, value: 84 }, { name: 130, value: 117 }, { name: 131, value: 137 }, { name: 132, value: 19 }, { name: 133, value: 23 }, { name: 134, value: 178 }, { name: 135, value: 16 }, { name: 136, value: 15 }, { name: 137, value: 197 }, { name: 138, value: 19 }, { name: 139, value: 200 }, { name: 140, value: 117 }, { name: 141, value: 196 }, { name: 142, value: 138 }, { name: 143, value: 137 }, { name: 144, value: 155 }, { name: 145, value: 41 }, { name: 146, value: 81 }, { name: 147, value: 40 }, { name: 148, value: 113 }, { name: 149, value: 149 }, { name: 150, value: 141 }, { name: 151, value: 71 }, { name: 152, value: 99 }, { name: 153, value: 124 }, { name: 154, value: 116 }, { name: 155, value: 129 }, { name: 156, value: 19 }, { name: 157, value: 50 }, { name: 158, value: 137 }, { name: 159, value: 45 }, { name: 160, value: 79 }, { name: 161, value: 18 }, { name: 162, value: 190 }, { name: 163, value: 100 }, { name: 164, value: 49 }, { name: 165, value: 9 }, { name: 166, value: 126 }, { name: 167, value: 21 }, { name: 168, value: 122 }, { name: 169, value: 198 }, { name: 170, value: 138 }, { name: 171, value: 81 }, { name: 172, value: 183 }, { name: 173, value: 0 }, { name: 174, value: 79 }, { name: 175, value: 163 }, { name: 176, value: 41 }, { name: 177, value: 124 }, { name: 178, value: 158 }, { name: 179, value: 135 }, { name: 180, value: 22 }, { name: 181, value: 145 }, { name: 182, value: 17 }, { name: 183, value: 102 }, { name: 184, value: 101 }, { name: 185, value: 100 }, { name: 186, value: 194 }, { name: 187, value: 44 }, { name: 188, value: 190 }, { name: 189, value: 157 }, { name: 190, value: 19 }, { name: 191, value: 119 }, { name: 192, value: 78 }, { name: 193, value: 87 }, { name: 194, value: 151 }, { name: 195, value: 60 }, { name: 196, value: 119 }, { name: 197, value: 165 }, { name: 198, value: 175 }, { name: 199, value: 10 }
      ]
    }
  ];

  constructor() { }

  ngOnInit() {
  }
}
