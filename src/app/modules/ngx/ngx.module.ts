import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxComponent } from './ngx.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Routes, RouterModule } from '@angular/router';
// import { BrowserAnimationsModule } from '@angular/platform-browser';

const routes: Routes = [
  {
    path:'', component: NgxComponent
  }
]

@NgModule({
  declarations: [NgxComponent],
  imports: [
    CommonModule,
    NgxChartsModule,
    // BrowserAnimationsModule,
    RouterModule.forChild(routes)
  ]
})
export class NgxModule { }
